@extends('adminlte.master')

@section('content')
    <div class="">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Pertanyaan Detail</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="inputName">Judul</label>
                    <input type="text" id="inputName" class="form-control" value="{{ $question->judul }}" readonly>
                </div>
                <div class="form-group">
                    <label for="inputDescription">Isi</label>
                    <textarea id="inputDescription" class="form-control"
                        rows="4" readonly>{{ $question->isi }}</textarea>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
@endsection
