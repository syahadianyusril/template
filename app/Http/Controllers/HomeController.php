<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('first');
    }
    
    public function master(){
        return view('adminlte/master');
    }
    public function index(){
        return view('table/index');
    }
}