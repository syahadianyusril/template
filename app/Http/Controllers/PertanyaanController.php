<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('questions.create');
    }

    public function store(Request $request){
        $request->validate([
            "judul" => "required|unique:questions",
            "isi" => "required"
        ]);

        $query = DB::table('questions')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success','Pertanyaan telah diterima');
    }

    public function index(){
        $questions = DB::table('questions')->get();
        return view('questions.index', compact('questions'));
    }

    public function show($id){
        $question = DB::table('questions')->where('id', $id)->first();
        return view('questions.show', compact('question'));
    }

    public function edit($id){
        $question = DB::table('questions')->where('id', $id)->first();
        return view('questions.edit', compact('question'));
    }

    public function update($id, Request $request){
        $request->validate([
            "judul" => "required|unique:questions",
            "isi" => "required"
        ]);


        $query = DB::table('questions')
            ->where('id', $id)
            ->update([
                "judul" => $request["judul"],
                "isi" => $request["isi"]
            ]);
        
        return redirect('/pertanyaan')->with('success','Berhasil update pertanyaan');
    }

    public function destroy($id){
        $query = DB::table('questions')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success','Berhasil hapus pertanyaan');
    }
}
